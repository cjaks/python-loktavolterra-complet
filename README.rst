Python Lokta Voltera modele
***************************

loktaVoltera est un projet qui permet de faire l'étude d'un système proie-prédateur avec le modèle de Lokta-Volterra

----

:Documentation: https://cjaks.gitlab.io/python-loktavolterra-complet
:Compatibility: Python 3.4+
:Licence:       GNU GPLv3 

----

Installation
************

Pour utiliser ce projet il faut cloner le repository

.. code:: bash

    #Get the source
    git clone https://gitlab.com/cjaks/python-loktavolterra-complet.git

Environnement virtuel
*********************

Pour travailler sur ce projet je vous conseil de travailler dans un environnement virtuel

.. code:: bash
    
    #Use Makefile
    make venv
    #or
    python3 -m venv venv
    source ./venv/bin/activate
    pip install -r requirements.txt

Tests
*****

Pour tester le module loktavolterra

.. code:: bash
    
    #Use Makefile
    make test
    #or
    python loktavolterra/test.py

