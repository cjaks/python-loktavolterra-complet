Introduction
------------

En mathématiques, les équations de prédation de Lotka-Volterra, que l'on désigne aussi sous le terme de « modèle proie-prédateur », sont un couple d'équations différentielles non-linéaires du premier ordre, et sont couramment utilisées pour décrire la dynamique de systèmes biologiques dans lesquels un prédateur et sa proie interagissent. Elles ont été proposées indépendamment par Alfred James Lotka en 19251 et Vito Volterra en 19262.

Ce système d'équations est classiquement utilisé comme modèle pour la dynamique du lynx et du lièvre des neiges, pour laquelle de nombreuses données de terrain ont été collectées sur les populations des deux espèces par la Compagnie de la baie d'Hudson au XIXe siècle. Il a aussi été employé par Allan Hobson pour décrire les relations entre les neurones cholinergiques responsables du sommeil paradoxal et les neurones aminergiques liées à l'état de veille.

source : <https://fr.wikipedia.org/>

Équations
---------

Le système proie-predateur lokta-volterra pour une population de proie :math:`x(t)` et de predateur :math:`y(t)`, s'écrit sous la forme suivante :

.. math:: 
    \begin{align} 
    \frac{\mathrm{d}x(t)}{\mathrm{d}t} &= x(t)\big(a - b y(t) \big) \\
    \frac{\mathrm{d}y(t)}{\mathrm{d}t} &= -y(t)\big(c - d x(t) \big) 
    \end{align}
    \iff \frac{\mathrm{d}}{\mathrm{d}t}X(t) = F(X(t))

Avec :

* :math:`a` : Taux de reproduction des proies

* :math:`b` : Taux de mortalité des proies

* :math:`c` : Taux de mortalité des prédateurs

* :math:`d` : Taux de reproduction des prédateurs

Équilibre des populations 
-------------------------

Les points d'équilibres du système :math:`\frac{\mathrm{d}}{\mathrm{d}t}X = F(X)` sont les points où les populations de proie et de prédateur n'évoluent plus i.e : 

.. math:: 
    \frac{\mathrm{d}}{\mathrm{d}t}X(t) = 0 \iff F(X(t))=0 \iff    
    \begin{align}
    x(t)\big(a - b y(t) \big) &= 0 \\
    -y(t)\big(c - d x(t) \big) &= 0
    \end{align}

Les solutions de ce sytème sont :

.. math::

    (x_{eq},y_{eq}) = \left\{(0,0),\left(\frac{c}{d},\frac{a}{b}\right)\right\}
