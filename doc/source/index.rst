.. loktaVoltera documentation master file, created by
   sphinx-quickstart on Tue Jun 18 14:59:46 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. toctree::
    :maxdepth: 2
    :numbered:
    :caption: Modele Lokta
    
    intro

.. toctree::
    :maxdepth: 2
    :caption: API
    
    API

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
