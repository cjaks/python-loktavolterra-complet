Classe `ProiePredateur`
-----------------------

.. automodule:: proiepredateur
    :members:

Classe `ParamNum`
-----------------------

.. automodule:: paramnum
    :members:

Classe `Etude`
-----------------------

.. automodule:: etude
    :members:

Module Analyse
-----------------------

.. automodule:: analyse
    :members:
