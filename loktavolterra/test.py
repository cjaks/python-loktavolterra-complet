
import unittest
import proiepredateur
import paramnum
import etude
import analyse

class TestLoktaVoltera(unittest.TestCase):
    def setUp(self):
        self.pp = proiepredateur.ProiePredateur();
        self.pn = paramnum.ParamNum();
        self.e = etude.Etude(pp=self.pp,pn=self.pn);
        return;        

    def test_global(self):
        self.pp.printPP();
        self.pn.printPN();
        X = self.e.integration(1.0,1.0);
        [statProie,statPreda] = analyse.stats(X);
        analyse.plotX(X,self.pn.t);
        return;
        
    def test_etude_pointFixe(self):
        X=self.e.pointFixe()
        Xtheo = [(0,0),(self.pp.c/self.pp.d, self.pp.a/self.pp.b)]
        self.assertListEqual(X,Xtheo);
        return;

if __name__ =="__main__":
    unittest.main();
