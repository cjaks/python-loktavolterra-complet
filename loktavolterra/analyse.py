# conding: utf-8

import matplotlib.pyplot as plt
import numpy as np

font = {'family'  : 'serif',
        'weight'  : 'bold',
        'size'    :  15}

plt.rc('text',usetex=True)
plt.rc('font',**font)

def stats(X):
    """
    Fonction qui permet de calculer des statistiques sur les résultats obtenu par la classe `Etude`
    
    Parameters
    ----------
    X : :obj:`numpy.array`
        Population des proies et des prédateurs
    
    Returns
    -------

    Examples
    --------
    >>> import proiepredateur
    >>> import paramnum
    >>> import etude
    >>> import analyse
    >>> pp = proiepredateur.ProiePredateur()
    >>> pn = paramanum.ParamNum()
    >>> e = etude.Etude(pp=pp,pn=pn)
    >>> X = e.integration(1.0,1.0)
    >>> Xeq = e.pointFixe()
    >>> [statsProie,statPreda]=analyse.stats(X)
    ==========================
    Satistiques : proie
    Mean     : 2.20
    Median   : 1.47
    std      : 1.82
    variance : 3.33
    ==========================
    Satistiques : predateur
    Mean     : 2.93
    Median   : 1.99
    std      : 2.20
    variance : 4.83
    """
    proieStats = ['proie',np.mean(X[:,0]), np.median(X[:,0]), np.std(X[:,0]), np.var(X[:,0])]
    predaStats = ['predateur',np.mean(X[:,1]), np.median(X[:,1]), np.std(X[:,1]), np.var(X[:,1])]

    def printStats(S):
        print("==========================")
        print("Satistiques : {0}".format(S[0]));
        print("Mean     : {0:.2f}".format(S[1]));
        print("Median   : {0:.2f}".format(S[2]));
        print("std      : {0:.2f}".format(S[3]));
        print("variance : {0:.2f}".format(S[4]));

    printStats(proieStats);
    printStats(predaStats);

    return proieStats, predaStats

def plotX(X,t,i=0):
    """
    Fonction qui permet de tarcer les résultats obtenus

    Parameters
    ----------
    X : :obj:`numpy.array`
        Population des proies et des prédateurs

    t : :obj:`numpy.array`
        Discrétisation temporelle
    """
    fig, ax = plt.subplots(2,1,tight_layout=True)

    ax[0].plot(t,X[:,0],label='proie')
    ax[0].plot(t,X[:,1],label='predateur')
    ax[0].legend()
    ax[0].set_title('Evolution temprorelle des populations de proies et de predateur')
    ax[0].set_xlabel(r'$t$')
    ax[0].set_ylabel('population')

    ax[1].plot(X[:,0],X[:,1])
    ax[1].set_title('Population de predateur en fonction de la population de proie')
    ax[1].set_xlabel(r'$x$ population proie')
    ax[1].set_ylabel(r'$y$ population prédateur')

    plt.show()
