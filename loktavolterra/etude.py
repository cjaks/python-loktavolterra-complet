# coding: utf-8

import proiepredateur
import paramnum

import numpy
import sympy
import scipy.integrate as integrate

class Etude(object):
    """
    classe `Etude` qui permet de resoudre et de trouver les points fixes de l'EDO
    
    Parameters
    ----------
    pp : object
        Objet de la classe `ProiePredateur`
    pn : object
        Objet de la classe `ParamNum`
    """
    def __init__(self,pp=proiepredateur.ProiePredateur(),pn=paramnum.ParamNum()):
        self.pp = pp;
        self.pn = pn;
        return;
    
    def integration(self,x0,y0):
        r"""
        Resoultion numérique du système d'EDO :math:`\frac{d}{dt}X=F(X)`

        Parameters
        ----------
        x0 : float
            Population initial de proie 
        y0 : float
            Population initiale de prédateur

        Returns
        -------
        numpy.array:
            Evolution des populations de proie et de prédateur jusqu'au temps d'intégration tf
        
        Examples
        --------
        >>> import etude
        >>> e = etude.Etude()
        >>> X = e.integration(1.0,1.0)    
        >>> print(X)
        [[1.         1.        ]
        [1.04109905 0.98055976]
        [1.08429665 0.96230977]
        ...
        [2.19586261 7.36922574]
        [2.01137438 7.38435221]]        
        """
        
        CI = numpy.array([x0,y0]);
        X = integrate.odeint(self.pp.equation,CI,self.pn.t);
        return X;
    
    def pointFixe(self):
        r"""
        Détermine les points fixes (d'équilibres) du système d'EDO :math:`F(X)=0`

        Returns
        -------
        list:
            Liste des points d'équilibres du système
        
        Examples
        --------
        >>> import etude
        >>> e = etude.Etude()
        >>> Xeq = e.pointFixe()
        ==========================
        [(0.0, 0.0), (2.00000000000000, 3.00000000000000)]
        """
        x, y = sympy.symbols('x y');
        X = [x,y];
        F = self.pp.equation(X);
        Xeq = sympy.solve(F,X);
        print("==========================");
        print("Les points fixes sont : {0}".format(Xeq));
        return Xeq;

if __name__ == "__main__":
    e = Etude();
    X = e.integration(1.0,1.0);
    Xeq = e.pointFixe();
    print(X);
