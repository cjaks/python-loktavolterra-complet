# coding: utf-8
import numpy

class ParamNum(object):
    """
    classe `ParamNum` qui définit les paramètres d'étude numérique

    Parameters
    ----------
    N : int
        Nombre de point de discretisation temporelle
    tf : float
        Temps d'intégration
    t : numpy.array
        Discrétisation temporelle 
    """
    def __init__(self,N=500,tf=10.0):
        self.N = N;
        self.tf = tf;
        self.t = numpy.linspace(0,self.tf,self.N);
        return;

    def modifN(self,N):
        """
        Modification des attributs N et t

        Parameters
        ----------
        N : int
            Nombre de point de discrétisation
        """
        self.N = N
        self.t = numpy.linspace(0,self.tf,N);
        return;

    def printPN(self):
        """
        Affiche les attributs de la classe
        
        Examples
        --------
        >>> import paramnum
        >>> pn = paramnum.ParamNum()
        >>> pn.printPN()
        ==========================
        Parametres numeriques
        - N  = 500  : Nombre de point de discrétisation temporelle
        - tf = 10.0 : Temps integration
        """
        print("==========================");
        print("Parametres numeriques");
        print("- N  = {N}  : Nombre de point de discrétisation temporelle".format(N=self.N));
        print("- tf = {tf} : Temps integration".format(tf=self.tf));
        
if __name__ == "__main__":
    pn = ParamNum();
    pn.printPN()
    
