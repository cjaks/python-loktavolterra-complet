# coding: utf-8

class ProiePredateur(object):
    r"""
    classe `ProiePredateur` qui définit les paramètres du système proie-predateur et le type de modélisation
    
    Parameters
    ----------
    a : float
        Taux de reproduction des proies
    b : float
        Taux de mortalité des proies
    c : float
        Taux de mortalité des prédateurs
    d : float
        Taux de reproduction des prédateurs
    name : str
        Nom du système proie-prédateur étudié
    equation : methode
        Second membre de l'équation :math:`\frac{d X}{d t}=F(X)``
    """
    def __init__(self,a=3.0,b=1.0,c=2.0,d=1.0,name='',modele=1):
        self.a = a;
        self.b = b;
        self.c = c;
        self.d = d;
        self.name = name;
        MODELE = {1:self.loktaVolterra};
        self.equation = MODELE[modele];
        return;

    def loktaVolterra(self,X,t=0):
        r"""
        Renvoi le terme F (lokta-volterra) de l'équation :math:`\frac{dX}{dt} = F(X)` 

        Parameters
        ----------
        X : numpy.array
            tableau numpy qui contient la population de proie et de prédateur à un instant 
        t : numpy.array
            discretisation temporelle

        Returns
        -------
        list : 
            Terme F(X)
        """
        F = [0,0];
        x = X[0];
        y = X[1];
        F[0] = x*(self.a - self.b*y);
        F[1] = -y*(self.c - self.d*x);
        return F;

    def printPP(self):
        """
        Affiche les attributs de la classe
        
        Examples
        --------
        >>> import proiepredateur
        >>> pp = proiepredateur.ProiePredateur()
        >>> pp.printPP()
        ==========================
        Proies-Predateurs :
        - a = 3.0 : taux de reproduction des proies
        - b = 1.0 : taux de mortalite des proies
        - c = 2.0 : taux de mortalite des predateurs
        - d = 1.0 : taux de reproduction des predateurs        
        """
        print("==========================")
        print("Proies-Predateurs : {name}".format(name=self.name));
        print("- a = {a} : taux de reproduction des proies".format(a=self.a));
        print("- b = {b} : taux de mortalite des proies".format(b=self.b));
        print("- c = {c} : taux de mortalite des predateurs".format(c=self.c));
        print("- d = {d} : taux de reproduction des predateurs".format(d=self.d));
        return;

if __name__ == "__main__":
    pp = ProiePredateur();
    pp.printPP()
