SHELL := /bin/zsh

venv:
	python3 -m venv venv
	source ./venv/bin/activate
	pip install -r requirements.txt

build_sphinx:
	sphinx-build -d ./doc/build/doctree doc/source -b html ./doc/build/html

test:
	python loktavolterra/test.py
